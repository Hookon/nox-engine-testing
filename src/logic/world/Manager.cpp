/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/Manager.h>

#include <nox/logic/IContext.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/world/event/ActorRemoved.h>
#include <nox/util/algorithm.h>
#include <list>
#include <cassert>

namespace nox { namespace logic { namespace world
{

Manager::Manager(IContext* context):
	context(context),
	eventBroadcaster(context->getEventBroadcaster()),
	log(context->createLogger()),
	actorFactory(this->context),
	actorUpdateLocked(false)
{
	assert(this->context != nullptr);
	assert(this->eventBroadcaster != nullptr);

	this->log.setName("WorldManager");
}

Manager::~Manager()
{
	if (this->saveThread.joinable() == true)
	{
		this->saveThread.join();
	}
}

void Manager::onUpdate(const Duration& deltaTime)
{
	this->actorUpdateLocked = true;

	std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);

	for (auto& actor : this->actors)
	{
		if (actor->isActive() == true)
		{
			actor->update(deltaTime);
		}
	}

	actorLock.unlock();

	this->actorUpdateLocked = false;
}

void Manager::reset()
{
	std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);
	this->nonPhysicalActorTransforms.clear();

	this->handleQueuedTasks();

	while (this->actorCreations.empty() == false)
	{
		this->actorCreations.pop();
	}

	this->actorRemovalQueue.clear();

	this->clearActors();
	this->actorFactory.resetCounter();
}

void Manager::saveEverything(const std::string& saveDirectory, SaveCompleteCallback completeCallback)
{
	if (this->saveThread.joinable() == true)
	{
		this->saveThread.join();
	}

	this->saveThread = std::thread(&Manager::saveWorld, this, completeCallback, saveDirectory);
}

void Manager::saveWorld(SaveCompleteCallback completeCallback, const std::string& saveDirectory)
{
}

void Manager::loadActorDefinitions(app::resource::IResourceAccess* resourceAccess, const std::string& definitionPath)
{
	this->actorFactory.loadActorDefinitions(resourceAccess, definitionPath);
}

actor::Actor* Manager::findActor(const actor::Identifier& id) const
{
	std::lock_guard<std::mutex> idMapLock(this->actorIdMutex);

	auto actorItr = this->idToActorMap.find(id);

	if (actorItr != this->idToActorMap.end())
	{
		return actorItr->second;
	}

	return nullptr;
}

std::vector<actor::Actor*> Manager::findActorsWithinRange(const glm::vec2& position, const float range) const
{
	std::vector<actor::Actor*> actorsInRange;

	std::unique_lock<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	for (const auto& actorTransform : this->nonPhysicalActorTransforms)
	{
		if (glm::distance(position, actorTransform->getPosition()) <= range)
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	transformLock.unlock();

	const math::Box<glm::vec2> rangeAabb(position - glm::vec2(range / 2.0f), position + glm::vec2(range / 2.0f));

	std::vector<actor::Actor*> physicsActorsWithin = this->context->getPhysics()->findActorsIntersectingAabb(rangeAabb, physics::BodyState::BOTH);
	util::mergeInto(physicsActorsWithin, this->context->getPhysics()->findEmptyActorsWithinAabb(rangeAabb, physics::BodyState::BOTH));

	for (actor::Actor* actor : physicsActorsWithin)
	{
		auto actorTransform = actor->findComponent<actor::Transform>();
		assert(actorTransform != nullptr);

		if (glm::distance(position, actorTransform->getPosition()) <= range)
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	return actorsInRange;
}

std::vector<actor::Actor*> Manager::findActorsWithinAxisAlignedBox(const math::Box<glm::vec2>& box) const
{
	std::vector<actor::Actor*> actorsInRange;

	std::unique_lock<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	for (const auto& actorTransform : this->nonPhysicalActorTransforms)
	{
		if (box.intersects(actorTransform->getPosition()))
		{
			actorsInRange.push_back(actorTransform->getOwner());
		}
	}

	transformLock.unlock();

	std::vector<actor::Actor*> physicsActorsWithin = this->context->getPhysics()->findActorsIntersectingAabb(box, physics::BodyState::BOTH);
	util::mergeInto(physicsActorsWithin, this->context->getPhysics()->findEmptyActorsWithinAabb(box, physics::BodyState::BOTH));

	actorsInRange.insert(actorsInRange.end(), physicsActorsWithin.begin(), physicsActorsWithin.end());

	return actorsInRange;
}

void Manager::onActorCreated(actor::Actor* actor)
{
	auto actorTransform = actor->findComponent<actor::Transform>();

	if (actorTransform != nullptr)
	{
		auto actorPhysics = actor->findComponent<physics::ActorPhysics>();

		if (actorPhysics == nullptr)
		{
			std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);
			this->nonPhysicalActorTransforms.push_back(actorTransform);
		}

		if (actor->hasParent() == false)
		{
			actor->create();
			actor->activate();
		}
	}
}

void Manager::setVisibleArea(const glm::vec2& position, const glm::vec2& size, const float rotation)
{
	this->onVisionChange(position, size, rotation);
}

std::unique_ptr<actor::Actor> Manager::createActorFromDefinitionName(const std::string& actorDefinitionName)
{
	std::vector<actor::Actor*> childActors;
	auto actor = this->actorFactory.createActor(actorDefinitionName, childActors);

	this->handleActorCreation(actor.get());

	return actor;
}

std::unique_ptr<actor::Actor> Manager::createActorFromSource(const Json::Value& jsonSource, const std::string& definitionName)
{
	std::vector<actor::Actor*> childActors;
	auto actor = this->actorFactory.createActor(jsonSource, definitionName, childActors);

	this->handleActorCreation(actor.get());

	return actor;
}

actor::Actor* Manager::manageActor(std::unique_ptr<actor::Actor>&& actor)
{
	actor::Actor* actorReference = actor.get();

	std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);
	this->actors.push_back(std::move(actor));
	actorLock.unlock();

	this->onActorCreated(actorReference);

	this->log.verbose().format("Actor %s with id %d managed.", actorReference->getName().c_str(), actorReference->getId());

	return actorReference;
}

void Manager::removeActor(std::unique_ptr<actor::Actor>&& actor)
{
	std::unique_lock<std::mutex> idMapLock(this->actorIdMutex);
	this->idToActorMap.erase(actor->getId());
	idMapLock.unlock();

	actor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
	actor->deactivate();
	actor->destroy();

	std::vector<std::unique_ptr<actor::Actor>> childActors = actor->detachAllChildActors();
	std::list<std::unique_ptr<actor::Actor>> detachedChildActorQueue;
	std::move(childActors.begin(), childActors.end(), std::back_inserter(detachedChildActorQueue));

	std::shared_ptr<ActorRemoved> removeEvent = std::make_shared<ActorRemoved>();
	removeEvent->setActor(std::move(actor));
	this->eventBroadcaster->queueEvent(removeEvent);

	while (detachedChildActorQueue.empty() == false)
	{
		std::unique_ptr<actor::Actor>& childActor = detachedChildActorQueue.front();

		std::vector<std::unique_ptr<actor::Actor>> childOfChildActors = childActor->detachAllChildActors();
		std::move(childOfChildActors.begin(), childOfChildActors.end(), std::back_inserter(detachedChildActorQueue));

		idMapLock.lock();
		this->idToActorMap.erase(childActor->getId());
		idMapLock.unlock();

		childActor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
		childActor->deactivate();
		childActor->destroy();

		std::shared_ptr<ActorRemoved> childRemoveEvent = std::make_shared<ActorRemoved>();
		childRemoveEvent->setActor(std::move(childActor));
		this->eventBroadcaster->queueEvent(childRemoveEvent);

		detachedChildActorQueue.pop_front();
	}

	this->onActorRemoved(removeEvent->getActor());
}

void Manager::removeActor(const actor::Identifier& actorId)
{
	if (this->actorUpdateLocked == true)
	{
		this->actorRemovalQueue.push(actorId);
	}
	else
	{
		std::unique_lock<std::mutex> actorLock(this->actorStorageMutex);

		auto actorItr = std::find_if(
				this->actors.begin(),
				this->actors.end(),
				[&actorId](const std::unique_ptr<actor::Actor> &actor)
				{
					return actorId == actor->getId();
				}
		);

		if (actorItr != this->actors.end())
		{
			this->actors.erase(actorItr);
			actorLock.unlock();

			this->removeActor(std::move(*actorItr));
		}
	}
}

void Manager::clearActors()
{
	for (auto& actor : this->actors)
	{
		actor->forceActiveState(actor::Actor::ForcedActiveState::NONE);
		actor->deactivate();
		actor->destroy();

		std::shared_ptr<ActorRemoved> removeEvent = std::make_shared<ActorRemoved>();
		removeEvent->setActor(std::move(actor));

		this->eventBroadcaster->queueEvent(removeEvent);

	}

	this->actors.clear();
	this->idToActorMap.clear();
}

void Manager::handleQueuedTasks()
{
	while (this->actorCreations.empty() == false)
	{
		std::unique_ptr<actor::Actor> actor = std::move(this->actorCreations.front());
		this->manageActor(std::move(actor));

		this->actorCreations.pop();
	}

	while (this->actorRemovalQueue.isEmpty() == false)
	{
		actor::Identifier id = this->actorRemovalQueue.pop(actor::Identifier());

		if (id != actor::Identifier())
		{
			this->removeActor(id);
		}
	}
}

void Manager::handleActorCreation(actor::Actor* actor)
{
	assert(actor != nullptr);

	actor::Identifier actorId = actor->getId();

	std::unique_lock<std::mutex> idMapLock(this->actorIdMutex);

	this->idToActorMap[actorId] = actor;

	const std::vector<actor::Actor::Child> children = actor->findChildrenRecursively();

	for (const actor::Actor::Child& child : children)
	{
		this->idToActorMap[child.actor->getId()] = child.actor;
	}

	idMapLock.unlock();

	auto actorCreatedEvent = std::make_shared<ActorCreated>(actor);

	this->eventBroadcaster->queueEvent(actorCreatedEvent);

	for (const actor::Actor::Child& child : children)
	{
		auto childActorCreatedEvent = std::make_shared<ActorCreated>(child.actor);

		this->eventBroadcaster->queueEvent(childActorCreatedEvent);
	}

	this->log.verbose().format("Created actor \"%s\" with id %d from definition \"%s\".", actor->getName().c_str(), actor->getId(), actor->getDefinitionName().c_str());
}

void Manager::loadWorld(const Json::Value& worldJson)
{
	const Json::Value& actorArrayValue = worldJson["actors"];

	std::vector<std::unique_ptr<actor::Actor>> createdActors;
	createdActors.reserve(actorArrayValue.size());

	for (const Json::Value& actorValue : actorArrayValue)
	{
		createdActors.push_back(this->createActorFromSource(actorValue, ""));
	}

	const Json::Value& viewArrayValue = worldJson["views"];

	// TODO: Parse and create Views.

	for (auto& actor : createdActors)
	{
		this->manageActor(std::move(actor));
	}
}

void Manager::onActorRemoved(actor::Actor* actor)
{
	const actor::Identifier& actorId = actor->getId();

	std::lock_guard<std::mutex> transformLock(this->nonPhysicalActorTransformsMutex);

	auto actorTransformIt = std::find_if(
			this->nonPhysicalActorTransforms.begin(),
			this->nonPhysicalActorTransforms.end(),
			[actorId](actor::Transform* transform)
			{
				return actorId == transform->getOwner()->getId();
			}
	);

	if (actorTransformIt != this->nonPhysicalActorTransforms.end())
	{
		this->nonPhysicalActorTransforms.erase(actorTransformIt);
	}
}

void Manager::loadInitialWorld(const glm::vec2& position, const glm::vec2 size)
{
}

void Manager::onVisionChange(const glm::vec2& position, const glm::vec2& size, const float rotation)
{
}

} } }
