/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/storage/DataStorageBoost.h>

#include <iterator>

namespace nox { namespace app
{
namespace storage
{

DataStorageBoost::DataStorageBoost()
{
}

void DataStorageBoost::init(const std::string& storageDirectory)
{
	std::string directoryPath = storageDirectory;

	// Remove all trailing slashes.
	while (directoryPath.back() == '/' || directoryPath.back() == '\\')
	{
		directoryPath.erase(directoryPath.size() - 1, 1);
	}

	this->dataStoragePath = directoryPath;
	this->dataStoragePath.make_preferred();

	if (boost::filesystem::exists(this->dataStoragePath) == false)
	{
		throw NonExistentPathException();
	}

	if (boost::filesystem::is_symlink(this->dataStoragePath) == true)
	{
		this->dataStoragePath = boost::filesystem::read_symlink(this->dataStoragePath);
	}

	if (boost::filesystem::is_directory(this->dataStoragePath) == false)
	{
		throw NotADirectoryException();
	}
}

bool DataStorageBoost::fileExists(const std::string& filePath) const
{
	auto completePath = this->dataStoragePath;
	completePath /= filePath;

	this->ensureDirectoryExistence(completePath);

	return boost::filesystem::exists(completePath);
}

void DataStorageBoost::openReadableFile(const std::string& filePath, std::ifstream& inputStream)
{
	auto completePath = this->dataStoragePath;
	completePath /= filePath;

	inputStream.open(completePath.string());
}

void DataStorageBoost::openWritableFile(const std::string& filePath, std::ofstream& outputStream, bool append)
{
	auto completePath = this->dataStoragePath;
	completePath /= filePath;

	this->ensureDirectoryExistence(completePath);

	std::ios_base::openmode mode = std::ios_base::out;

	if (append == true)
	{
		mode |= std::ios_base::app;
	}

	outputStream.open(completePath.string(), mode);
}

std::string DataStorageBoost::readFileContent(const std::string& filePath)
{
	auto completePath = this->dataStoragePath;
	completePath /= filePath;

	std::ifstream fileStream(completePath.string());

	if (fileStream)
	{
		std::istreambuf_iterator<char> fileStart(fileStream);
		std::istreambuf_iterator<char> fileEnd;

		return std::string(fileStart, fileEnd);
	}
	else
	{
		return std::string();
	}
}

void DataStorageBoost::writeFileContent(const std::string& filePath, const std::string& content, bool append)
{
	auto completePath = this->dataStoragePath;
	completePath /= filePath;

	this->ensureDirectoryExistence(completePath);

	std::ios_base::openmode mode = std::ios_base::out;

	if (append == true)
	{
		mode |= std::ios_base::app;
	}

	std::ofstream fileStream(completePath.string(), mode);

	if (fileStream)
	{
		fileStream << content;
	}
}

void DataStorageBoost::removeFolder(const std::string &directoryPath)
{
	auto completePath = this->dataStoragePath;
	completePath /= directoryPath;
	
	boost::filesystem::remove_all(completePath);
}

void DataStorageBoost::ensureDirectoryExistence(const boost::filesystem::path& filePath) const
{
	boost::filesystem::path dirPath = filePath.parent_path();

	if (boost::filesystem::exists(dirPath) == false)
	{
		boost::filesystem::create_directories(dirPath);
	}
}

const char* DataStorageBoost::IntializationException::what() const NOX_NOEXCEPT
{
	return "Failed to initialize.";
}

const char* DataStorageBoost::NonExistentPathException::what() const NOX_NOEXCEPT
{
	return "Path does not exist on the filesystem.";
}

const char* DataStorageBoost::NotADirectoryException::what() const NOX_NOEXCEPT
{
	return "Path is not a directory.";
}

}
} }
