/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/util/math/angle.h>
#include <glm/common.hpp>

namespace nox { namespace math
{

float clampAngle(float value)
{
	while (value < -glm::pi<float>())
	{
		value += 2.0f * glm::pi<float>();
	}

	while (value >= glm::pi<float>())
	{
		value -= 2.0f * glm::pi<float>();
	}

	return value;
}

float calculateAngleDifference(float firstAngle, float secondAngle)
{
	float difference = secondAngle - firstAngle;

	return clampAngle(difference);
}

// Original code from https://bitbucket.org/pekaaw/imt3601_gameprogramming/src/89ca761fa6c694fb051a011715c168745356ea3c/src/util/eigen.h?at=master
float clampAnglePositive(float value)
{
	value = glm::mod(value, glm::pi<float>() * 2.0f);
	return value;
}

bool angleIsWithinCone(float angle, float coneMinAngle, float coneMaxAngle)
{
	angle = clampAngle(angle);
	coneMinAngle = angle + calculateAngleDifference(angle, coneMinAngle);
	coneMaxAngle = angle + calculateAngleDifference(angle, coneMaxAngle);

	if (angle >= coneMinAngle && angle <= coneMaxAngle)
	{
		return true;
	}
	else
	{
		return false;
	}
}

} }
