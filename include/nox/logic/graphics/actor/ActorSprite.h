/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_GRAPHICS_ACTORSPRITE_H_
#define NOX_LOGIC_GRAPHICS_ACTORSPRITE_H_

#include <nox/logic/graphics/actor/ActorGraphics.h>

namespace nox
{

namespace app { namespace graphics
{

class SpriteRenderNode;

} }

namespace logic { namespace graphics
{

class ActorSprite: public ActorGraphics
{
public:
	const static IdType NAME;

	const IdType& getName() const override;
	std::vector<IdType> findInheritedNames() const override;

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;

	/**
	 * Sets and, if necessary, updates the renderlevel of the spriteRenderNode.
	 * @param renderLevel the new level to set for the sprite.
	 */
	void setRenderLevel(unsigned int renderLevel);

	void addSprite(const std::string& spriteName);
	void setActiveSprite(const std::string& spriteName);

	void setCenter(const glm::vec2& center);

private:
	std::shared_ptr<nox::app::graphics::SceneGraphNode> createSceneNode() override;
	void onColorChange(const glm::vec4& color) override;
	void onLightMultiplierChange(const float lightMultiplier) override;
	void onEmissiveLightChange(const float emissiveLight) override;

	std::string spriteName;
	unsigned int renderLevel;
	glm::vec2 center;

	std::shared_ptr<nox::app::graphics::SpriteRenderNode> spriteRenderNode;
};

} } }

#endif
