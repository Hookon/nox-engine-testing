/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_SDLAPPLICATION_H_
#define NOX_APP_SDLAPPLICATION_H_

#include "Application.h"

#include <SDL_events.h>
#include <SDL.h>

namespace nox { namespace app
{

class SdlApplication: public Application
{
public:
	explicit SdlApplication(const std::string& applicationName, const std::string& organizationName);

	SdlApplication(SdlApplication&& other);
	SdlApplication& operator=(SdlApplication&& other);

	bool initializeSdlSubsystem(const Uint32 subsystemFlag);
	void destroySdlsubSystem(const Uint32 subsystemFlag);

	/**
	 * Get the path to a directory where the application is allowed to write and read data.
	 * This directory could contain config files, save files, etc.
	 *
	 * If the path does not exist on the filesystem, the path will be created. The path will contain a combination
	 * of the organization name and application name passed to the SdlApplication constructor.
	 *
	 * @param allowSpaces If this is set to false, spaces will be removed from the application and
	 * organization name for the path.
	 *
	 * @return The path to the storage directory. If this is empty, no path could be found or created.
	 */
	std::string getStorageDirectoryPath(const bool allowSpaces = false) const;

	virtual bool onInit() override;
	virtual void onDestroy() override;
	virtual void onUpdate(const Duration& deltaTime) override;
	virtual void onEvent(const SDL_Event& event);

private:
	log::Logger log;
};

} }

#endif
