/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TILEDTEXTUREGENERATOR_H_
#define NOX_APP_GRAPHICS_TILEDTEXTUREGENERATOR_H_

#include <vector>
#include <string>
#include <GL/glew.h>

#include "opengl/TextureQuad.h"

namespace nox { namespace app
{
namespace graphics
{

class TextureManager;
class TiledTextureRenderer;

class TiledTextureGenerator
{
public:
	TiledTextureGenerator(const TextureManager& textureManager, TiledTextureRenderer* renderer);
	
	virtual ~TiledTextureGenerator();

	bool addTextureTile(const std::string& textureName);

	void setTileSize(const glm::vec2& size);

	void setTileColor(const glm::vec4& color);

	/**
	 * If there is the need, updates the texture generated for the world.
	 * @param currentPosition the position in the world that the center is at.
	 * @param currentCoverage the total width and height the area will cover in meters.
	 */
	virtual void onUpdate(glm::vec2 currentPosition, glm::vec2 currentCoverage);
	
protected:
	void setTileRenderer(TiledTextureRenderer* renderer);
	bool needToUpdateTexture(glm::vec2 lowerLeftBounds, glm::vec2 upperRightBounds);
	void generateTexture();

private:
	const TextureManager& textureManager;

	TiledTextureRenderer* tileRenderer;

	std::vector<TextureQuad> textures;
	glm::vec2 quadSize;
	glm::vec4 tileColor;
	glm::vec2 tileLightLuminance;

	std::vector<TextureQuad::RenderQuad> textureQuads;
	std::vector<GLuint> indices;

	glm::ivec2 currentMinTiles;
	glm::ivec2 currentMaxTiles;
	
	const float SMALL_OVERLAP;
};

}
} }

#endif
