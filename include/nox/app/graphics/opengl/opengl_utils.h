/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file opengl_utils.h
 * @brief OpenGL utility functions.
 */

#ifndef NOX_APP_GRAPHICS_OPENGLUTILS_H_
#define NOX_APP_GRAPHICS_OPENGLUTILS_H_

#include <GL/glew.h>
#include <string>
#include <fstream>
#include <vector>

namespace nox { namespace app
{
namespace graphics
{

class GlslShader
{
public:
	void create(const GLenum type);
	bool compile(const std::string& source);
	bool compile(const char* source);
	void destroy();

	GLuint getId() const;
	bool isCreated() const;
	bool isCompiled() const;
	std::string getCompileLog() const;

private:
	GLuint id = 0;
	bool compiled = false;
};

/**
 * Manages the lifetime of an OpenGL framebuffer object (FBO) and its
 * renderbuffers.
 *
 * When destroyed, the framebuffer and renderbuffers will be deleted.
 */
class FrameBuffer
{
public:
	FrameBuffer();
	FrameBuffer(const GLuint name, const std::vector<GLuint>& renderBuffers);

	FrameBuffer(const FrameBuffer& other) = delete;
	FrameBuffer& operator=(const FrameBuffer& other) = delete;

	FrameBuffer(FrameBuffer&& other);
	FrameBuffer& operator=(FrameBuffer&& other);

	~FrameBuffer();

	GLuint getId() const;

	void bind(const GLenum target);

private:
	GLuint bufferName = 0;
	std::vector<GLuint> renderBuffernNames;
};

/**
 * Note: It should actually be possible to iterate through the color attachments
 * using GL_COLOR_ATTACHMENT0 + i, but I find this nicer as it's more clear what is going on.
 * Also, GLEW/OpenGL might change in the future.
 */
const GLenum COLOR_ATTACHMENTS[] =
{
	GL_COLOR_ATTACHMENT0,
	GL_COLOR_ATTACHMENT1,
	GL_COLOR_ATTACHMENT2,
	GL_COLOR_ATTACHMENT3,
	GL_COLOR_ATTACHMENT4,
	GL_COLOR_ATTACHMENT5,
	GL_COLOR_ATTACHMENT6,
	GL_COLOR_ATTACHMENT7,
	GL_COLOR_ATTACHMENT8,
	GL_COLOR_ATTACHMENT9,
	GL_COLOR_ATTACHMENT10,
	GL_COLOR_ATTACHMENT11,
	GL_COLOR_ATTACHMENT12,
	GL_COLOR_ATTACHMENT13,
	GL_COLOR_ATTACHMENT14,
	GL_COLOR_ATTACHMENT15
};

/**
 * Create and compile shader from the content in the provided string.
 * @param shaderContent The shader code to compile.
 * @param shaderType The shader type to compile.
 * @return The ID of the created shader. 0 if failed.
 */
GlslShader createShader(const std::string& shaderContent, const GLenum shaderType);

/**
 * Create and compile shader from the content in the provided file stream.
 * @param shaderFile File stream with code to compile.
 * @param shaderType The shader type to compile.
 * @return The ID of the created shader. 0 if failed.
 */
GlslShader createShader(std::istream& shaderFile, const GLenum shaderType);

/**
 * Generate best possible GLSL version string from the comment and current GL version.
 * The string will be of the format "#version xxx\n".
 * @param requiredVersionString The string for the version required ("xxx")
 * @param currentGlMajor The current GL major version.
 * @param currentGlMinor The current GL minor version.
 * @return The GLSL version string.
 */
std::string generateGlslVersionString(const std::string& requiredVersionString, const GLuint currentGlMajor, const GLuint currentGlMinor);

/**
 * Create and compile shader from the content in the provided file stream and automatically find version.
 * Does the same as createShader(std::fstream& shaderFile, GLenum shaderType)
 * but will automatically find the best GLSL version from the comment "//? required xxx"
 * @param shaderFile File stream with code to compile.
 * @param shaderType The shader type to compile.
 * @param currentGlMajor The current GL major version.
 * @param currentGlMinor The current GL minor version.
 * @return The ID of the created shader. 0 if failed.
 */
GlslShader createShaderAutoVersion(std::istream& shaderFile, const GLenum shaderType, const GLuint currentGlMajor, const GLuint currentGlMinor);

/**
 * Link shader program from vertex and fragment shaders.
 * @param shaderProgram Program to link to. Must be created beforehand and not be 0.
 * @param vertexShader Vertex shader to link.
 * @param fragmentShader Fragment shader to link.
 * @return true if linking succeeded, otherwise false.
 */
bool linkShaderProgram(const GLuint shaderProgram, const GLuint vertexShader, const GLuint fragmentShader);

/**
 * Check the linking status of program and log errors.
 * @param program Program to check.
 * @return true if OK, false if not.
 */
bool checkLinkStatus(const GLuint program);

/**
 * Check compile status of shader and log errors.
 * @param shaderId Shader to check.
 * @return true if OK, false if not.
 */
bool checkCompileStatus(const GLuint shaderId);

std::string getShaderCompileLog(const GLuint shaderId);

void fillQuadIndexVector(std::vector<GLuint>& indexVector, const unsigned int numQuads, const unsigned int startQuadNum = 0);

void resizeQuadIndexVector(std::vector<GLuint>& indexVector, const unsigned int newSize);

GLuint createTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLint internalFormat = GL_RGBA, const GLenum format = GL_RGBA, const GLint filtering = GL_LINEAR);

GLuint createMultisampleTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLsizei samples);

FrameBuffer createFrameBufferObject(GLsizei FBOWidth, GLsizei FBOHeight, const std::vector<GLuint>& textures = std::vector<GLuint>(), bool hasDepthAndStencilBuffer = false);

FrameBuffer createMultisampleFrameBufferObject(const GLsizei FBOWidth, const GLsizei FBOHeight, const std::vector<GLuint>& textures = std::vector<GLuint>(), const GLsizei samples = 2, const bool hasDepthAndStencilBuffer = false);

void handleAsyncDataCopy(const void* data, const GLsizeiptr& dataSize, const GLbitfield invalidationFlag = GL_MAP_INVALIDATE_BUFFER_BIT, const GLenum target = GL_ARRAY_BUFFER);

void handleAsyncDataCopy(const void* firstData, const GLsizeiptr& firstDataSize, const void* secondData, const GLsizeiptr& secondDataSize, const GLbitfield invalidationFlag = GL_MAP_INVALIDATE_BUFFER_BIT, const GLenum target = GL_ARRAY_BUFFER);

/**
 * Deletes a framebuffer and sets the name to 0, indicating that it is
 * not longer referring to a valid buffer.
 *
 * This only deletes the framebuffer is the name is not 0.
 */
void deleteFramebufferIfValid(GLuint& name);

/**
 * Deletes a texture and sets the name to 0, indicating that it is
 * not longer referring to a valid texture.
 *
 * This only deletes the texture is the name is not 0.
 */
void deleteTextureIfValid(GLuint& name);

}
} }

#endif
