/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_DIRECTORY_H_
#define NOX_APP_RESOURCE_DIRECTORY_H_

#include <nox/app/resource/Descriptor.h>
#include <nox/common/api.h>

#include <vector>
#include <string>

namespace nox { namespace app
{
namespace resource
{

/**
 * Representation of a directory for resources or subdirectories.
 */
class NOX_API Directory
{
public:
	Directory();

	/**
	 * Construct a directory.
	 * @param name Name of the directory.
	 */
	Directory(const std::string& name);

	//! Add a subdirectory to this directory.
	void addSubdirectory(const Directory& subdirectory);

	//! Add a resource to this directory.
	void addResource(const Descriptor& resource);

	//! Get the name of the directory.
	const std::string& getName() const;

	//! Get all directories below this.
	const std::vector<Directory>& getSubdirectories() const;

	//! Get all resources within this directory.
	const std::vector<Descriptor>& getResources() const;

private:
	std::string name;
	std::vector<Directory> subirectories;
	std::vector<Descriptor> resources;
};

}
} }

#endif
