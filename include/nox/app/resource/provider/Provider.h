/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_PROVIDER_H_
#define NOX_APP_RESOURCE_PROVIDER_H_

#include <nox/app/resource/Directory.h>
#include <nox/common/api.h>

#include <string>

namespace nox { namespace app { namespace resource
{

class Descriptor;

/**
 * Provides access to resources from a repository.
 */
class NOX_API Provider
{
public:
	virtual ~Provider();

	/**
	 * Get the top directory of the provider.
	 */
	const Directory& getTopDirectory() const;

	/**
	 * Open the provider for use.
	 * @pre The provider must have been initialized to its repository.
	 * @post The provider can be used to access resources.
	 * @return true if successfully opened, otherwise false.
	 */
	virtual bool open() = 0;

	/**
	 * Get the size of a raw resource.
	 * @param resource Resource to get the size of.
	 * @return The size of the raw resource in bytes, or 0 if resource not found.
	 */
	virtual unsigned int getRawResourceSize(const Descriptor& resource) = 0;

	/**
	 * Get the raw resource.
	 * @param resource Resource to get.
	 * @param[out] buffer Buffer to store the raw resource in. Must be equal or larger than the result of getRawResourceSize().
	 * @return The size of the raw resource in bytes.
	 */
	virtual unsigned int getRawResource(const Descriptor& resource, char* buffer) = 0;

	/**
	 * Get the number of resources that can be accessed.
	 */
	virtual unsigned int getNumResources() const = 0;

	/**
	 * Get the path to a resource.
	 * @param resourceNumber Resource number to get (see getNumResources()).
	 * @return Path to resource, or empty string if invalid number.
	 */
	virtual std::string getResourcePath(unsigned int resourceNumber) const = 0;

protected:
	Directory topDirectory;
};

}
} }

#endif
